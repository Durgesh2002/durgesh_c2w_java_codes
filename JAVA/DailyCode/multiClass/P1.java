class One{

	int x=10;
	static int y=20;
	
	void run(){
		System.out.println("In Run Method");
	}
	static void fun(){
		System.out.println("In Fun Method");
	}

}
class Two{
	public static void main(String []args){
		One obj=new One();
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.run();
		obj.fun();
	}
}

