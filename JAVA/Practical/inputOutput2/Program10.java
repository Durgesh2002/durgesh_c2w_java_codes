import java.io.*;
class Program10{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());
		
		int count=0;
		char ch='a';
		for(int i=1;i<=row;i++){
			ch=(char)(count+'a');
			int num=1+count;	
			for(int j=1;j<=i;j++){
				if(i%2==0){
			
					System.out.print(ch+" ");
					ch++;
				}
				else{
			
					System.out.print(num+" ");
					num++;
				}
				count++;
			}
			System.out.println();
		}
	}
}
