import java.io.*;
class Program9{
	public static void main(String[]arh)throws IOException{
		BufferedReader BT=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BT.readLine());
		
		char ch='a';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(row+1 +" ");
				}
				else{
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
