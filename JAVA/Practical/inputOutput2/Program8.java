import java.io.*;
class Program8{
	public static void main(String[]args)throws IOException{
		BufferedReader rr=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(rr.readLine());

		char ch='c';
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=1;j<=i;j++){
				if(j%2==0){
					System.out.print(ch+" ");
					ch+=2;
					
				}
				else{
					System.out.print(num+" ");
					num+=2;
				}
			
			}
			System.out.println();
		}
	}
}
