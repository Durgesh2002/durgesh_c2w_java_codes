import java.io.*;
class Program7{
	public static void main(String[]args)throws IOException{
		BufferedReader P7=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(P7.readLine());

		char ch='a';
		int num=1;
		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){
				if(j%2==0){
					System.out.print(ch+" ");
					ch++;
				}
				else{
					System.out.print(num +" ");
				}
			}
			num++;
			System.out.println();
		}
	}
}
