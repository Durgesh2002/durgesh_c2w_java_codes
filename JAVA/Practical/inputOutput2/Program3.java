import java.io.*;
class Program3{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());

		for(int i=1;i<=row;i++){
			char ch=(char)(65+row-1);
			for(int j=1;j<=i;j++){
				System.out.print(ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}
