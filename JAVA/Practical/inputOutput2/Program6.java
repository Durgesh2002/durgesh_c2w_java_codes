import java.io.*;
class Program6{
	public static void main(String[]D)throws IOException{
		BufferedReader bb=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(bb.readLine());

		int count=0;
		char ch='A';
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num++ +" ");
				
				}
				else{
					System.out.print((char)(65+count)+" ");
				}
				count++;
			}
			System.out.println();
		}
	}
}
