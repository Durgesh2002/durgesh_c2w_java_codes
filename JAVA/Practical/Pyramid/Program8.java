import java.io.*;
class Program8{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int row=Integer.parseInt(br.readLine());
		int num=row;
		for (int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}

			for(int j=1;j<=i*2-1;j++){
				System.out.print(num+"\t");
			}
			System.out.println();
			num--;
		}
	}
}


