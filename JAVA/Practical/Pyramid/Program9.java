import java.util.*;
class Program9{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			char ch1='a';
			char ch2='A';
			for(int j=1;j<=i*2-1;j++){
				if(i%2==0)
					if(j<i)
						System.out.print(ch1++ +"\t");
					else
						System.out.print(ch1-- +"\t");
				else 
					if(j<i)
						System.out.print(ch2++ +"\t");
					else
						System.out.print(ch2-- +"\t");
				}

			
			System.out.println();
		}
	}
}
