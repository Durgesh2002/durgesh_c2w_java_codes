import java.io.*;
class Program4{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}

			for(int j=1;j<=i*2-1;j++){
				System.out.print((char)(64+i)+"\t");
			}
			System.out.println();
		}
	}
}

