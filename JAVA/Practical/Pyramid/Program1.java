import java.io.*;
class Program1{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		
		
		for(int i=1;i<=row;i++){
			
			for(int sp=row;sp>i;sp--){
				System.out.print("\t");
			}

			for(int j=1;j<=i*2-1;j++){
				System.out.print(1+"\t");
			}
			System.out.println();
		}
	}
}

