import java.io.*;
class P3{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int num=0;
		int temp=0;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				num++;
			}
			else{
				num--;
			}
			temp=num;
			for(int j=1;j<=num;j++){
				System.out.print(temp+"\t");
				temp--;
			}
			System.out.println();
		}
	}
}
		
