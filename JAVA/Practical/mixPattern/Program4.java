import java.io.*;
class Program4{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num*j+" ");
			}
			System.out.println();
			num--;
		}
	}
}
