import java.util.*;
class Program6{
	public static void main(String[]args){
		Scanner Sc=new Scanner(System.in);

		int row=Sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=row;
			for (int j=1;j<=i;j++){

				if(i%2==0){
					System.out.print(num+" ");
					num--;
				}
				else{
					System.out.print(((char)(96+num))+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}

