import java.io.*;
class Program3{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());
		
		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){

				if(i%2 != 0){
					System.out.print((char)(64+row-j+1)+" ");
				}
				else{
					System.out.print(j+" ");
				}
			}
			System.out.println();
		}
	}
}
				
