import java.io.*;
class Program1{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());

		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
