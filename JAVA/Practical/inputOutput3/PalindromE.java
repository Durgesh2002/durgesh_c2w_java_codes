import java.io.*;
class PalindromE{
	public static void main(String[]args)throws IOException{

		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));

		int num=Integer.parseInt(BR.readLine());

		int temp=num;
		int rem=0;
		int rev=0;

		while(temp>0){
			rem=temp%10;
			rev=rev*10+rem;
			temp=temp/10;
			
		}
		System.out.println(rev);
		if(rev==num){
			System.out.println(num+" is Palindrome");
		}
		else{
			System.out.println(num+" is not Palindrome");
		}
	}
}
