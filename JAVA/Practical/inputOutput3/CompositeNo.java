import java.io.*;
class CompositeNo{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int num=Integer.parseInt(BR.readLine());

		int count=0;
		for(int i=num;i>0;i--){
			if(num%i==0){
				count++;
			}
		}
		if(count>2){
			System.out.println(num+"Composite Number");
		}
		else{
			System.out.println(num+"Not Composite Number");
		}
	}
}
