import java.io.*;
class P8{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());

		int arr[][]=new int[row][col];
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		int temp=1;
		int sum=0;
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				if(j==(row-temp)){
					sum=sum+arr[i][j];
				}
			}
			temp++;
		
		}
		System.out.println(sum);
	}
}

