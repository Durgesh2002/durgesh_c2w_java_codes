import java.io.*;
class Prog9{
	public static void main(String[]args)throws IOException{
		InputStreamReader ist=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(ist);

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int j=0;j<arr.length;j++){
			if(j%2==1){
				System.out.println(arr[j]+" is a odd indexed element");
			}
		}
	}
}

