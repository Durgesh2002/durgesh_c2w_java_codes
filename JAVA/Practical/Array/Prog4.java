import java.io.*;
class Prog4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a arry size");
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int sum=0;
		for(int j=0;j<arr.length;j++){
			if(arr[j]%2!=0){
				sum=sum+arr[j];
			}
		}
		System.out.println("Array of odd no sum = "+sum); 
	}
}
		
