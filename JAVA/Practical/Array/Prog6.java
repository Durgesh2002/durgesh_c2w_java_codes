import java.io.*;
class Prog6{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a array size");
		int size=Integer.parseInt(br.readLine());

		char[] arr=new char[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=br.readLine().charAt(0);
		}

		for(int j=0;j<arr.length;j++){
			System.out.println(arr[j]);
	}
	}
}
