import java.io.*;
class Program8{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			char ch=(char)(64+row);
			for(int j=1;j<=row;j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print("#"+" ");
					}
					else{
						System.out.print(ch+" ");
						ch--;
					}
				}
				else{
					if(j%2==1){
						System.out.print(ch+" ");
						ch--;
					}
					else{
						System.out.print("#"+" ");
					}

				}
						
			}
			System.out.println();
		}
	}
}


