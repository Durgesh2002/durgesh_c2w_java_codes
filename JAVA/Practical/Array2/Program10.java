import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
class Program10{
	public static void main(String []args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for (int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int num=arr[0];
		int ind=0;
		for (int j=1;j<arr.length;j++){
			if(arr[j]>num){
				num=arr[j];
				ind=j;
			}
		}
		System.out.println(num+" are found in index "+ind);
	}
}
