import java.io.*;
class Program2{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum=0;
		for(int j=0;j<arr.length;j++){
			if(arr[j]%3==0){
				System.out.print(arr[j]+" ");
				sum=sum+arr[j];
			}
		}
		System.out.println(sum);
	}
}
			
