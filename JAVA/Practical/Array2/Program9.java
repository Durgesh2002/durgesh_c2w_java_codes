import java.io.*;
class Program9{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		for (int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int num=arr[0];

		for(int j=1;j<arr.length;j++){
			if(arr[j]<arr[0]){
				num=arr[j];
			}
		}
		System.out.println(num);
	}
}
