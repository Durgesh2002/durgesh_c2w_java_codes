import java.io.*;
class Program6{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int pro=1;
		for(int j=0;j<arr.length;j++){
			if(j%2==1){
				pro=pro*arr[j];
			}
		}
		System.out.println("Product are"+pro);
	}
}
