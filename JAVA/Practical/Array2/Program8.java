import java.io.*;
class Program8{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int j=0;j<arr.length;j++){
			if(arr[j]>5 && arr[j]<9){
				System.out.println(arr[j]+" is greater than 5 but less than 9");
			}
		}
	}
}

