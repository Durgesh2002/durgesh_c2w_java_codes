import java.io.*;
import java.util.*;
class Question3{
	public static void main(String []args){
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferReader br=new BufferReader(isr);
		int num=br.nextInt();

		if(num%8==0){
			System.out.println(num+" is divisible by 8");
		}
		else{
			System.out.println(num+" is not divisible by 8");
		}
	}
}
