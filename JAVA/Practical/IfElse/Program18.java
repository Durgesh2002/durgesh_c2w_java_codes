class Program18
{
	public static void main(String []args)
	{
		float marks=85.00f;

		if(marks<=100 && marks>=90){
			System.out.println("first class with distinction");
		}
		else if(marks>=80 && marks<90){
			System.out.println("First class");
		}
		else if(marks>=60 && marks<80){
			System.out.println("Second class");
		}
		else if(marks>=35 && marks<60){
			System.out.println("third class");
		}
		else{
			System.out.println("Fail");
		}
	}
}
