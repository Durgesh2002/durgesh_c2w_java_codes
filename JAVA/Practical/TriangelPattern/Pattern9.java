import java.io.*;
class Pattern10{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row :");
		int row=Integer.parseInt(BR.readLine());
	
		for(int i=1;i<=row;i++){
			char ch=(char)(64+i);
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1 && j%2==1){
					System.out.print((int)ch++ +" ");
				}
				else if(i%2==0 && j%2==0){
					System.out.print((int)ch++ +" ");
				}
				else{
					System.out.print(ch++ +" ");
				}
			}
			System.out.println();
			
		}
	}
}	
