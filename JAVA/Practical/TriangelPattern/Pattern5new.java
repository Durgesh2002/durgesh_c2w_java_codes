import java.io.*;
class Pattern5new{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a rows :");
		int row=Integer.parseInt(BR.readLine());

		for (int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(i*j+" ");
			}
			System.out.println();
		}
	}
}
