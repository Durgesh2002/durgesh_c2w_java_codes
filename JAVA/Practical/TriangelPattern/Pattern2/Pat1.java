class Pattern {
    public static void printPattern(int row) {
        int currentLetter = 'A' + row - 1;
        
        for (int i = 0; i < row; i++) {
            StringBuilder letters = new StringBuilder();
            for (int j = 0; j < row - i; j++) {
                letters.append((char)currentLetter).append(" ");
                currentLetter--;
            }
            System.out.println(letters);
        }
    }

    public static void main(String[] args) {
        printPattern(3);
        System.out.println();
        printPattern(4);
    }
}

