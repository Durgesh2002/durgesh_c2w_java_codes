import java.util.*;
class Program6{
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);

		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			char ch=0;	
			int num=row-i+1;
			for(int j=1;j<=num;j++) {
				
				if(i%2==0){
					System.out.print((char)(ch+97)+" ");
					ch++;
				}
				else{
					System.out.print((char)(ch+65)+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
