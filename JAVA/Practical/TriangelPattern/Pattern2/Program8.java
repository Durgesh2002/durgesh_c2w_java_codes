import java.util.*;
class Program8{
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			
			int num=row-i+1;
			
			for(int j=1;j<=row-i+1;j++){

				if(j%2==0){
					System.out.print((char)(96+num)+" ");
					
				}
				else{
					System.out.print(num+" ");
					
				}
				num--;
			}
			System.out.println();
		}
	}
}
