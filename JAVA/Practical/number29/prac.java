import java.io.*;
class prac{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int num=Integer.parseInt(br.readLine());
		int sqr=num*num;
		int temp=num;
		int cnt=0;
		while(temp>0){
			temp/=10;
			cnt++;
		}
		int rem=0;
		int rev=0;
		while(cnt>0){
			rem=sqr%10;
			rev=rev*10+rem;
			sqr/=10;
			cnt--;
		}
		int orev=0;
		while(rev>0){
			rem=rev%10;
			orev=orev*10+rem;
			rev/=10;
		}
		if(orev==num)
			System.out.println("Automorphic");
		else
			System.out.println("Not Automorphic");
	}
}

