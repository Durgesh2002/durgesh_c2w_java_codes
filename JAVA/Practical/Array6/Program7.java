import java.io.*;
class Program7{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]>=65 && arr[i]<=90){
				System.out.println((char)arr[i]);
			}
			else{
				System.out.println(arr[i]);
			}
		}
	}
}
