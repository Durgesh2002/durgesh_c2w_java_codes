import java.io.*;
class Program1{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			for(int space=1;space<=row-i;space++){
				System.out.print(" "+"\t");
			}
			
			for(int j=1;j<=i;j++){
				System.out.print("*"+"\t");
			}
			System.out.println();
		}
	
	}
}

