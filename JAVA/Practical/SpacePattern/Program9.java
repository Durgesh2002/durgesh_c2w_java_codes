import java.io.*;
class Program9{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			for(int space=1;space<i;space++){
				System.out.print(" "+"\t");
			}
			int num=row;
			for(int j=1;j<=row-i+1;j++){
				System.out.print((char)(64+num)+"\t");
				num--;
			}
			System.out.println();
		}
	}
}
