import java.io.*;
class Program3{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());

		int col=0;
		for(int i=1;i<row*2;i++){
		
			if(i<=row){
				col++;
			}
			else{
				col--;
			}
			int num=col;
			for(int j=1;j<=col;j++){
				System.out.print(num+" ");
				num--;
			}
			System.out.println();
		}
	}
}
