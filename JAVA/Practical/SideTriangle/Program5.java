import java.io.*;
class Program5{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());

		int col=0;
		int col2=row+1;

		for(int i=1;i<row*2;i++){

			if(i<=row){
				col++;
				col2--;
			}
			else{
				col2++;
				col--;
			}
			for(int j=1;j<=col;j++){
				System.out.print((char)(64+col2)+"\t");
			}
			System.out.println();
		}
	}
}
						
